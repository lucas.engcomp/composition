package application;

import entities.Department;
import entities.HourContract;
import entities.Worker;
import enums.WorkerLevel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) throws ParseException {
        Locale.setDefault(Locale.US);
        Scanner scanner = new Scanner(System.in);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        System.out.println("Informe o nome do departamento: ");
        String departmentName = scanner.nextLine();

        System.out.println("Dados do trabalhador: ");
        System.out.println("Informe o nome do trabalhador");
        String workerName = scanner.nextLine();
        System.out.println("Informe o nível do trabalhador");
        String workerLevel = scanner.nextLine();
        System.out.println("Informe o salário do trabalhador");
        Double baseSalary = scanner.nextDouble();

        //instanciando dados do trabalhador
        Worker worker = new Worker(workerName, WorkerLevel.valueOf(workerLevel), baseSalary, new Department(departmentName));

        System.out.println("Quantos contratos terá esse trabalhador?");
        int n = scanner.nextInt();

        for (int i = 1; i < n; i++) {
            System.out.println("Entre com o contrato #" + i + " e seus dados:");
            System.out.print("Informe a data");
            Date contractDate = sdf.parse(scanner.next());

            System.out.print("Informe o valor por hora: ");
            double valuePerHour = scanner.nextDouble();

            System.out.println("Duração do contrato: ");
            int hours = scanner.nextInt();

            //instancia o contrato
            HourContract contract = new HourContract(contractDate, valuePerHour, hours);
            //associa o contrato ao trabalhador
            worker.addContract(contract);
        }

        System.out.println();
        System.out.println("Informe o mes e ano para calcular o salário (MM/yyyy) ");
        String monthAndYear = scanner.next();
        //recorta a string informada, gerando um substring
        int month = Integer.parseInt(monthAndYear.substring(0, 2));
        int year = Integer.parseInt(monthAndYear.substring(3));

        System.out.println("Nome: " + worker.getName());
        System.out.println("Departamento: " + worker.getDepartment().getName());
        System.out.println("No periodo de " + monthAndYear + worker.getName() + " ganhou" + String.format("%.2f", worker.income(year, month)));
    }

}
