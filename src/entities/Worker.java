package entities;

import enums.WorkerLevel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Worker {

    private String name;
    private WorkerLevel level;
    private Double baseSalary;

    //associações
    /** 1 departamento
     * */
    private Department department;
    /** vários contratos representado por uma lista
     * */
    private List<HourContract> contracts = new ArrayList<>();

    public Worker() {
    }

    public Worker(String name, WorkerLevel level, Double baseSalary, Department department) {
        this.name = name;
        this.level = level;
        this.baseSalary = baseSalary;
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WorkerLevel getLevel() {
        return level;
    }

    public void setLevel(WorkerLevel level) {
        this.level = level;
    }

    public Double getBaseSalary() {
        return baseSalary;
    }

    public void setBaseSalary(Double baseSalary) {
        this.baseSalary = baseSalary;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public List<HourContract> getContracts() {
        return contracts;
    }

    public void setContracts(List<HourContract> contracts) {
        this.contracts = contracts;
    }

    public void addContract(HourContract contract) {
        //adiciona um contrato a lista de contratos
        contracts.add(contract);
    }

    public void removeContract(HourContract contract) {
        //remove o contrato que veio como argumento do método.
        contracts.remove(contract);
    }

    public double income(int year, int month) {
        double sum = baseSalary;
        Calendar calendar = Calendar.getInstance();

        for (HourContract c : contracts) {
            //pega a data do contrato e definiu como sendo a data do calendário
            calendar.setTime(c.getDate());
            //atribui o ano usando o Calendar
            int c_year = calendar.get(Calendar.YEAR);
            //atribui o mes usando o Calendar
            int c_month = 1 + calendar.get(Calendar.MONTH);

            if (year == c_year && month == c_month) {
                sum += c.totalValue();
            }
        }
        return sum;
    }
}

